<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\OwnerController;

Route::middleware(['web', 'auth'])->group(function () {
    Route::post('/logout', [UserController::class, 'logout'])->name('logout');
});

Route::middleware(['web', 'auth', 'customer'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
    Route::get('/dashboard/bookedRoom', [DashboardController::class, 'booked'])->name('dashboard.booked');
    Route::get('/dashboard/profile', [DashboardController::class, 'profile'])->name('dashboard.profile');
    Route::get('/dashboard/{id}/showRoom', [DashboardController::class, 'showRoom'])->name('dashboard.showRoom');
    Route::get('/dashboard/{id}/{book_id}/showRoomBooked', [DashboardController::class, 'ShowBookedRoom'])->name('dashboard.showBookedRoom');
    Route::put('/dashboard/{id}/updateProfile', [DashboardController::class, 'updateProfile'])->name('dashboard.updateProfile');
    Route::post('/dashboard/{id}/bookRoom', [DashboardController::class, 'bookRoom'])->name('dashboard.bookRoom');
    Route::put('/dashboard/{id}/destroy', [DashboardController::class, 'destroy'])->name('dashboard.destroy');
    Route::put('/dashboard/{id}/cancel', [DashboardController::class, 'cancel'])->name('dashboard.cancel');
    Route::post('/dashboard/changePassword', [UserController::class, 'changePassword'])->name('dashboard.changePassword');
});

Route::middleware(['web', 'auth', 'owner'])->group(function () {
    Route::get('/owner', [OwnerController::class, 'index'])->name('owner.index');
    Route::get('/owner/AddRoom', [OwnerController::class, 'showAdd'])->name('owner.add');
    Route::get('/owner/{id}/updateRoom', [OwnerController::class, 'showUpdate'])->name('owner.showUpdate');
    Route::post('/owner/AddRoom', [OwnerController::class, 'store']);
    Route::get('/owner/{id}/showRoom', [OwnerController::class, 'showRoom'])->name('owner.showRoomDetails');
    Route::get('/owner/reservationDetails', [OwnerController::class, 'reservationDetails'])->name('owner.reservationDetails');
    Route::put('/owner/{id}/hideRoom', [OwnerController::class, 'hideRoom'])->name('owner.hideRoom');
    Route::put('/owner/{id}/destroy', [OwnerController::class, 'destroy'])->name('owner.destroy');
    Route::post('/owner/{id}/update', [OwnerController::class, 'update'])->name('owner.update');
    Route::get('/owner/profile', [OwnerController::class, 'profile'])->name('owner.profile');
    Route::put('/owner/{id}/destroyProfile', [OwnerController::class, 'destroyProfile'])->name('owner.destroyProfile');
    Route::put('/owner/{id}/updateProfile', [OwnerController::class, 'updateProfile'])->name('owner.updateProfile');
    Route::post('/owner/changePassword', [UserController::class, 'changePassword'])->name('owner.changePassword');
});

Route::middleware(['web', 'guest'])->group(function () {
    Route::get('/register', [UserController::class, 'showSignupForm'])->name('register')->middleware('redirectIfAuthenticated');
    Route::post('/register', [UserController::class, 'register']);
    Route::get('/login', [UserController::class, 'showLoginForm'])->name('login')->middleware('redirectIfAuthenticated');
    Route::post('/login', [UserController::class, 'login']);
    Route::get('/', [UserController::class, 'Welcome'])->name('welcome')->middleware('redirectIfAuthenticated');
});
