<?php

namespace App;

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    // Fillable attributes
    protected $fillable = [
        'user_id',
        'room_id',
        'checkin_date',
        'checkout_date',
        'rent',
        'booked',
    ];

    // Relationship: Each booking belongs to a user
    public function user()
    {
        return $this->belongsTo(Auth::class, 'user_id');
    }

    // Relationship: Each booking belongs to a room
    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id');
    }
}
