<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Auth extends Authenticatable
{
    use HasFactory, Notifiable;
    // Database table associated with this model
    protected $table = 'auths';
    protected $fillable = [
        'username',
        'email',
        'phone',
        'password',
        'role',
    ];

    // Attributes that should be hidden when converted to an array or JSON
    protected $hidden = [
        'password',
        'remember_token',
    ];

    // Define a one-to-many relationship with the Booking model
    public function bookings()
    {
        return $this->hasMany(Booking::class, 'user_id');
    }

    // Define a one-to-many relationship with the Image model
    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
