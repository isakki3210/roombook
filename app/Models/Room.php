<?php

namespace App;

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{

    // Fillable attributes
    protected $fillable = [
        'user_id',
        'min_availability_per_person',
        'max_availability_per_person',
        'room_name',
        'location',
        'address',
        'cost',
        'type_of_property',
        'floor_size',
        'no_of_beds',
        'aminities',
        'available',
        'description',
    ];

    // Relationship: Each room has many images
    public function images()
    {
        return $this->hasMany(Image::class);
    }

    // Relationship: Each room has many bookings
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    // Relationship: Each room belongs to a user
    public function user()
    {
        return $this->belongsTo(Auth::class, 'user_id');
    }
}
