<?php

namespace App;

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    // Fillable attributes
    protected $fillable = ['image'];

    // Relationship: Each image belongs to a room
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    // Relationship: Each image belongs to a user
    public function user()
    {
        return $this->belongsTo(Auth::class, 'user_id');
    }
}
