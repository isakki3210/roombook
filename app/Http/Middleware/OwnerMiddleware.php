<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as FacadeAuth;

class OwnerMiddleware
{
    // Function for handle the owner middleware ie, IF the OWner is login he/she doesn't able to move on the Customer Related Pages
    // This will be done by this Middleware 
    public function handle(Request $request, Closure $next)
    {
        $user = FacadeAuth::user();

        if ($user && $user->role === 'owner') {
            return $next($request);
        }

        return redirect()->route('dashboard.index');
    }
}
