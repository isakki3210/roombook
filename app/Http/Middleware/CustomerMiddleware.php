<?php

namespace App\Http\Middleware;

use Closure;

class CustomerMiddleware
{
    // Function for handle the Customer middleware ie, IF the Customer is login he/she doesn't able to move on the Owner Related Pages
    // This will be done by this Middleware 
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->role === 'customer') {
            return $next($request);
        }
        return redirect()->route('owner.index');
    }
}
