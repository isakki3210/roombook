<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    // Function for handle Basic things ie, The user will successfully Loged in
    // He/She Doesn't be able to move on to the login or any other pages without Logout
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $user = Auth::user();
            switch ($user->role) {
                case 'customer':
                    return redirect('/dashboard');
                case 'owner':
                    return redirect('/owner');
                default:
                    return redirect('/');
            }
        }
        return $next($request);
    }
}
