<?php

namespace App\Http\Controllers;

use App\Models\Auth;
use Illuminate\Http\Request;
use App\Models\Room;
use App\Models\Booking;
use Illuminate\Support\Facades\Auth as FacadeAuth;
use Carbon\Carbon;

// This is a Dashboard Controller for Handle the user Details..
class DashboardController extends Controller
{
    // This is a public function for index page
    // It will Room::all() function to get all the rooms
    // Get the users for the room owner name phone..

    public function index()
    {
        $rooms = Room::all();
        $users = Auth::whereIn('id', $rooms->pluck('user_id'))->get(['id', 'username', 'phone']);
        return view('dashboard.index', compact('rooms', 'users'));
    }

    // This is also a function to show the details of the particular room..
    // It shows booked and cancel reservation
    public function showBookedRoom($id, $book_id)
    {
        $currentUser = FacadeAuth::user();
        // Find the room
        $room = Room::find($id);
        // Find the users associated with the room
        $users = Auth::whereIn('id', [$room->user_id])->get(['id', 'username', 'phone', 'email']);
        // Find the booking based on both room_id and the provided booking ID
        $booking = Booking::where('room_id', $room->id)
            ->where('id', $book_id)
            ->where('user_id', $currentUser->id)
            ->first(['id', 'checkin_date', 'checkout_date', 'user_id', 'booked']);

        return view('dashboard.showBookedRoom', compact('room', 'users', 'booking'));
    }

    // This is a public function for Booked Room page
    // Get the users for the room owner name phone..
    // Only Booked Room of the User only Shows There..
    public function booked()
    {
        $rooms = Room::all();
        $users = Auth::whereIn('id', $rooms->pluck('user_id'))->get(['id', 'username', 'phone']);
        $bookings = Booking::where('user_id', FacadeAuth::id())->with('room')->get();
        return view('dashboard.bookedRoom', compact('rooms', 'users', 'bookings'));
    }

    // This is a public function for Profile page
    // It will shows the user details in the page
    public function profile()
    {
        $user = FacadeAuth::user();
        $bookings = Booking::where('user_id', FacadeAuth::id())->with('room')->get();
        return view('dashboard.showProfile', compact('user', 'bookings'));
    }

    // This is a Public function for destroy the user profile
    // While deactivate the account it will automatically deactivate the booked rooms of the user..
    public function destroy($id)
    {
        $profile = Auth::find($id);
        if ($profile) {
            $profile->bookings()->delete();
            $profile->delete();
            return redirect()->route('register')->with('success', 'Profile Deactivated successfully');
        }
    }

    // This is a function for update the profile details
    // That is.. update for username and phone of the user
    public function updateProfile(Request $request, $id)
    {
        $request->validate([
            'username' => 'required|string|max:255',
            'phone' => 'required|string|max:20',
        ]);
        $user = Auth::findOrFail($id);
        $user->update([
            'username' => $request->input('username'),
            'phone' => $request->input('phone'),
        ]);

        return redirect()->back()->with('success', 'Profile updated successfully');
    }

    // This is a function for shows the details of the rooms ie, Explore More..
    // particular room details will shows there.. from the id of the room.. 
    // get the bookings details of the room to show the not available dates to the user..
    public function showRoom($id)
    {
        $room = Room::find($id);
        $users = Auth::whereIn('id', [$room->user_id])->get(['id', 'username', 'phone', 'email']);
        $bookings = Booking::where('room_id', $room->id)->get(['id', 'checkin_date', 'checkout_date', 'user_id']);
        return view('dashboard.showRoom', compact('room', 'users', 'bookings'));
    }



    //This is a Function for cancel the reservation.. using IS of the Booking
    public function cancel($id)
    {
        $booking = Booking::find($id);
        $booking->update([
            'booked' => !$booking->booked,
        ]);
        return back()->with('success', 'Cancel reservation Successful.');
    }

    // This is a function for booked a room..
    // we will check the availability dates, calculate the costs and satisfy the minimum and maximum stay..
    public function bookRoom(Request $request, $roomId)
    {
        $request->validate([
            'checkInDate' => 'required|date',
            'checkOutDate' => 'required|date|after:checkInDate',
        ]);


        $checkInDate = $request->input('checkInDate');
        $checkOutDate = $request->input('checkOutDate');

        $bookingDuration = Carbon::parse($checkInDate)->diffInDays(Carbon::parse($checkOutDate));
        $room = Room::findOrFail($roomId);
        $totalCost = $bookingDuration * $room->cost;

        if ($bookingDuration < $room->min_availability_per_person || $bookingDuration > $room->max_availability_per_person) {
            return back()->with('error', 'The selected booking duration is not within the Available range. The Availability for a person was ' . $room->min_availability_per_person . ' to ' . $room->max_availability_per_person . ' Days');
        }

        $overlappingBookings = Booking::where('room_id', $roomId)
            ->where(function ($query) use ($checkInDate, $checkOutDate) {
                $query->whereBetween('checkin_date', [$checkInDate, $checkOutDate])
                    ->orWhereBetween('checkout_date', [$checkInDate, $checkOutDate])
                    ->orWhere(function ($query) use ($checkInDate, $checkOutDate) {
                        $query->where('checkin_date', '<=', $checkInDate)
                            ->where('checkout_date', '>=', $checkOutDate);
                    });
            })
            ->exists();

        if ($overlappingBookings) {
            return back()->with('error', 'The room is not available for the selected period.');
        }
        Booking::create([
            'user_id' => auth()->user()->id,
            'room_id' => $roomId,
            'checkin_date' => $checkInDate,
            'checkout_date' => $checkOutDate,
            'rent' => $totalCost,
        ]);
        return back()->with('success', 'Room booked successfully. The Total Amount will be Taken From Your Card was ' . $totalCost . ' Rupees for ' . $bookingDuration . ' Days');
    }
}
