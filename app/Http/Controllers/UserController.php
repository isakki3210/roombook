<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Auth;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class UserController extends Controller
{
    // function for SHOW The SIGNUP FORM
    public function showSignupForm()
    {
        return view('user.signup');
    }

    // Function to register the user and store it in an DB
    public function register(Request $request)
    {
        try {
            $request->validate([
                'username' => 'required|unique:auths',
                'email' => 'required|email|unique:auths',
                'phone' => 'required|unique:auths',
                'password' => 'required|min:6',
                'role' => 'required|in:customer,owner',
            ]);

            Auth::create([
                'username' => $request->input('username'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'password' => bcrypt($request->input('password')),
                'role' => $request->input('role'),
            ]);

            return redirect()->route('register')->with('success', 'Account created successfully! Please <a href="' . route('login') . '">login</a> using your credentials.');
        } catch (\Exception $e) {
            return redirect()->route('register')->with('error', 'An error occurred while creating the account.');
        }
    }

    // function for SHOW The Login FORM
    public function showLoginForm()
    {
        return view('user.login');
    }

    public function welcome()
    {
        return view('welcome');
    }

    // FUnction to Login SUCCESS OR NOT
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $user = Auth::where('email', $request->input('email'))->first();

        if ($user && password_verify($request->input('password'), $user->password)) {
            FacadesAuth::loginUsingId($user->id);
            if ($user->role === 'customer') {
                return redirect()->intended('/dashboard');
            } elseif ($user->role === 'owner') {
                return redirect()->intended('/owner');
            }
        } else {
            return redirect()->route('login')->with('error', 'Invalid email or password.');
        }
    }

    // Function for LOGOUT
    public function logout()
    {
        FacadesAuth::logout();
        return redirect()->route('login');
    }

    // Function for Change Password With Checking of the OLD PASS
    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|min:8|different:current_password',
            'confirm_password' => 'required|same:new_password',
        ]);

        $user = auth()->user();

        if (password_verify($request->current_password, $user->password)) {
            $user->update(['password' => bcrypt($request->new_password)]);
            return redirect()->back()->with('success', 'Password changed successfully.');
        }

        return back()->withErrors(['current_password' => 'The current password is incorrect.']);
    }
}
