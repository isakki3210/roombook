<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;
use App\Models\Room;
use App\Models\Auth;
use Illuminate\Support\Facades\Auth as FacadeAuth;
use Illuminate\Support\Str;

// OwnerController for handle the Owner Details in it..
class OwnerController extends Controller
{
    // This is a function to shows the user added rooms there..
    public function index()
    {
        $rooms = Room::where('user_id', FacadeAuth::id())->get();
        return view('owner.index', compact('rooms'));
    }

    // This is a function for profile
    // In this we can done actions for the owner profile..
    public function profile()
    {
        $user = FacadeAuth::user();
        return view('owner.showProfile', compact('user'));
    }

    // This is a function for destroy the profile..
    // If the Owner Destroy the profile it will delete the User related Room and images of the room 
    public function destroyProfile($id)
    {
        $admin = Auth::find($id);

        if ($admin) {
            $rooms = Room::where('user_id', $admin->id)->get();

            foreach ($rooms as $room) {
                $room->images()->delete();
                $room->delete();
            }
            $admin->delete();

            return redirect()->route('register')->with('success', 'Profile Deactivated successfully');
        }
    }

    // Function to update a profile
    public function updateProfile(Request $request, $id)
    {
        $request->validate([
            'username' => 'required|string|max:255',
            'phone' => 'required|string|max:20',
        ]);
        $user = Auth::findOrFail($id);
        $user->update([
            'username' => $request->input('username'),
            'phone' => $request->input('phone'),
        ]);

        return redirect()->back()->with('success', 'Profile updated successfully');
    }

    // Function for Show the ADD ROOM CONTENT
    public function showAdd()
    {
        return view('owner.add');
    }

    //it will shows only the particular details of the room using ID
    public function showRoom($id)
    {
        $room = Room::find($id);
        $bookings = Booking::where('room_id', $room->id)->get(['id', 'checkin_date', 'checkout_date', 'user_id', 'rent']);
        return view('owner.showRoomDetails', ['room' => $room, 'bookings' => $bookings]);
    }

    //Function for shows the reservation details with the users rooms
    // All the room booking of the particular user will shows there 
    public function reservationDetails()
    {
        $userRooms = Room::with('bookings')->where('user_id', FacadeAuth::id())->get();
        return view('owner.reservationDetails', compact('userRooms'));
    }

    //Function to shows the update details of the room Details 
    public function showUpdate($id)
    {
        $room = Room::find($id);
        return view('owner.update', ['room' => $room]);
    }

    // Function to delete the room While delete the room it will delete the images of the room also..
    public function destroy($id)
    {
        $room = Room::find($id);
        $name = $room->room_name;
        if (!$room) {
            return redirect()->route('owner.index', $id)->with('error', 'An Error Occurred');
        }
        $room->bookings()->delete();
        $room->images()->delete();
        $room->delete();
        return redirect()->route('owner.index')->with('success', $name . ' Room Deleted successfully!');
    }

    // Function to change the availability or unavailability of the Particular Room with ID
    public function hideRoom($id)
    {
        $room = Room::find($id);
        if (!$room) {
            return redirect()->route('owner.showRoomDetails', $id)->with('error', 'An Error Occurred');
        }
        $room->update(['available' => !$room->available]);
        $message = $room->available ? 'Room Available successfully' : 'Room Unavailable successfully';
        return redirect()->route('owner.showRoomDetails', $id)->with('success', $message);
    }

    // Function to create a room with images also.
    public function store(Request $request)
    {
        try {
            $request->validate([
                'room_name' => 'required|string',
                'location' => 'required|string',
                'floor_size' => 'required|string',
                'no_of_beds' => 'required|integer',
                'min_availability_per_person' => 'required|integer',
                'max_availability_per_person' => 'required|integer',
                'cost' => 'required|integer',
                'aminities' => 'required|string',
                'type_of_property' => 'required|string',
                'address' => 'required|string',
                'description' => 'required|string',
                'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $room = Room::create([
                'room_name' => $request->room_name,
                'location' => $request->location,
                'floor_size' => $request->floor_size,
                'no_of_beds' => $request->no_of_beds,
                'min_availability_per_person' => $request->min_availability_per_person,
                'max_availability_per_person' => $request->max_availability_per_person,
                'cost' => $request->cost,
                'aminities' => $request->aminities,
                'type_of_property' => $request->type_of_property,
                'address' => $request->address,
                'description' => $request->description,
                'user_id' => FacadeAuth::id(),
            ]);
            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $image) {
                    $imageName = Str::random(20) . '.' . $image->getClientOriginalExtension();
                    $image->storeAs('room_images', $imageName, 'public');
                    $room->images()->create(['image' => $imageName]);
                }
            }

            return redirect()->route('owner.add')->with('success', 'Room created successfully! You can Add Another Room also..');
        } catch (\Exception $e) {
            dd($e);
            return redirect()->route('owner.add')->with('error', 'An error occurred while creating the room.');
        }
    }

    // Function to update the room details
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'room_name' => 'required|string',
                'location' => 'required|string',
                'floor_size' => 'required|string',
                'no_of_beds' => 'required|integer',
                'min_availability_per_person' => 'required|integer',
                'max_availability_per_person' => 'required|integer',
                'cost' => 'required|integer',
                'aminities' => 'required|string',
                'type_of_property' => 'required|string',
                'address' => 'required|string',
                'description' => 'required|string',
            ]);

            $room = Room::find($id);

            $room->update([
                'room_name' => $request->room_name,
                'location' => $request->location,
                'floor_size' => $request->floor_size,
                'no_of_beds' => $request->no_of_beds,
                'min_availability_per_person' => $request->min_availability_per_person,
                'max_availability_per_person' => $request->max_availability_per_person,
                'cost' => $request->cost,
                'aminities' => $request->aminities,
                'type_of_property' => $request->type_of_property,
                'address' => $request->address,
                'description' => $request->description,
                'user_id' => FacadeAuth::id(),
            ]);

            return redirect()->route('owner.showRoomDetails', ['id' => $room->id])->with('success', 'Room Updated successfully!');
        } catch (\Exception $e) {
            dd($e);
            return redirect()->route('owner.showRoomDetails', ['id' => $room->id])->with('error', 'An error occurred while Updating the room.');
        }
    }
}
