<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('auths');
            $table->integer('min_availability_per_person');
            $table->integer('max_availability_per_person');
            $table->string('room_name');
            $table->string('location');
            $table->string('address');
            $table->integer('cost');
            $table->string('type_of_property');
            $table->string('floor_size');
            $table->integer('no_of_beds');
            $table->string('aminities');
            $table->boolean('available')->default(true);
            $table->text('description');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
