@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card shadow p-2">
                <div class="card-body">
                    <form action="{{ route('register') }}" method="post">
                        @csrf
                        <h3 class="mb-4 text-center">Register</h3>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                            <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="username">
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-envelope"></i></span>
                            <input type="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1" name="email">
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone"></i></span>
                            <input type="tel" class="form-control" placeholder="Phone" aria-label="Phone" aria-describedby="basic-addon1" name="phone">
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                            <input type="password" class="form-control" placeholder="Password" aria-label="password" aria-describedby="basic-addon1" name="password">
                        </div>
                        <select class="form-select mb-3" aria-label="Default select example" name="role">
                            <option selected disabled>Select Your Role</option>
                            <option value="customer">Customer</option>
                            <option value="owner">Owner</option>
                        </select>
                        <div class="d-grid gap-2">
                            <button class="btn btn-outline-primary" type="submit">Register</button>
                        </div>
                    </form> <br>
                    <p class="h6 text-center">If You have an account Please <a href="{{route('login')}}" style="text-decoration: none;">Login</p>
                    <br>
                    @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
                    @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection