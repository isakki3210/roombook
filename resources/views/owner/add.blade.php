@extends('layouts.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/dashboard">
            <i class="bi bi-house-lock-fill"></i> RoomBook
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown fs-5">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person-circle"></i> {{Auth::user()->username}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('owner.profile') }}">My Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('owner.reservationDetails') }}">Reservation Details</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item" type="submit"><i class="bi bi-box-arrow-left"></i> Logout</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container mt-5 card shadow-sm p-4">
    <div class="h4 mb-3">
        <i class="bi bi-house-add-fill"></i> Add Your Rooms
    </div>
    @if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    @if(session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <form action="{{ route('owner.add') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-row row mb-3">
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Room Name</label>
                <input type="text" class="form-control" id="input1" placeholder="Room Name" name="room_name">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Location</label>
                <input type="text" class="form-control" id="input1" placeholder="Enter Location of the Room (Chennai, India)" name="location">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Floor Size</label>
                <input type="text" class="form-control" id="input1" placeholder="Enter Flor Size (2 x 4)" name="floor_size">
            </div>
        </div>
        <div class="form-row row mb-3">
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Beds Available</label>
                <input type="text" class="form-control" id="input1" placeholder="Number of Beds Available" name="no_of_beds">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Minimum Availability Per Person</label>
                <input type="text" class="form-control" id="input1" placeholder="Enter Minimum Availability Per Person" name="min_availability_per_person">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Maximum Availability Per Person</label>
                <input type="text" class="form-control" id="input1" placeholder="Enter Maximum Availability Per Person" name="max_availability_per_person">
            </div>
        </div>
        <div class="form-row row mb-3">
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Room Rent (Per Day)</label>
                <input type="text" class="form-control" id="input1" placeholder="Enter Room Rent" name="cost">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Amenities of the Room</label>
                <input type="text" class="form-control" id="input1" placeholder="Enter Aminities of the Room (Water, current)" name="aminities">
            </div>

            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Room Image(s)</label>
                <small>Minimum 5 Image(s)</small>
                <input type="file" class="form-control" id="input1" name="images[]" multiple>
            </div>
        </div>
        <div class="form-row row mb-3">
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Type of Property</label>
                <input type="text" class="form-control" id="input1" placeholder="Enter Type of Property (Apartment, House..)" name="type_of_property">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Address</label>
                <textarea class="form-control" id="input1" placeholder="Enter the full Address" name="address"></textarea>
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Description</label>
                <textarea class="form-control" id="input1" placeholder="Description of the Room" name="description"></textarea>
            </div>
        </div>
        <div class="d-flex justify-content-end">
            <button class="btn btn-outline-success ml-auto mt-3" type="submit"><i class="bi bi-plus-lg"></i> Add Room</button>
        </div>
    </form>
</div>
@endsection