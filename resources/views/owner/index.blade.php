@extends('layouts.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/dashboard">
            <i class="bi bi-house-lock-fill"></i> RoomBook
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown fs-5">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person-circle"></i> {{Auth::user()->username}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('owner.profile') }}">My Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('owner.reservationDetails') }}">Reservation Details</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item" type="submit"><i class="bi bi-box-arrow-left"></i> Logout</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container mt-5">
    <p class="h4 mb-3 mx-1 text-dark font-weight-normal">Welcome Back, {{ Auth::user()->username }} </p>
    @if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    @if(session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <p class="h5 mb-3 mx-1 font-weight-normal">Add Your Rooms</p>
    <div class="card shadow-sm mb-4" style="width: 25rem;">
        <div class="card-body">
            <a href="{{ route('owner.add')}}" class="btn btn-outline-success stretched-link w-100"><i class="bi bi-house-add"></i> Add</a>
        </div>
    </div>
    <p class="h4 mb-3 mx-1 text-dark font-weight-normal">Your Rooms</p>
    <div class="row">
        <div class="row">
            @forelse ($rooms as $room)
            <div class="col-lg-4 mb-3">
                <div class="card shadow-sm" style="width: 25rem;">
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            @foreach ($room->images as $index => $image)
                            <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
                                <img src="{{ asset('storage/room_images/' . $image->image) }}" class="d-block w-100 rounded-top" alt="Room Image" height="300px">
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title"> {{ $room->room_name }}</h5>
                        <p class="card-text"><i class="bi bi-geo-alt-fill"></i> {{ $room->location }}</p>
                        <a href="{{ route('owner.showRoomDetails', $room->id) }}" class="btn btn-outline-primary stretched-link w-100"><i class="bi bi-info-circle"></i> Explore More</a>
                    </div>
                </div>
            </div>
            @empty
            <div class="col-lg-12 h3 mt-3 mx-1 text-secondary text-center">
                <p>😥No rooms added yet. Please Wait</a></p>
            </div>
            @endforelse
        </div>
    </div>
    @endsection