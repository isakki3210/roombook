@extends('layouts.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/dashboard">
            <i class="bi bi-house-lock-fill"></i> RoomBook
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown fs-5">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person-circle"></i> {{Auth::user()->username}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('owner.profile') }}">My Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('owner.reservationDetails') }}">Reservation Details</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item" type="submit"><i class="bi bi-box-arrow-left"></i> Logout</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container mt-5 card shadow-sm p-4">
    <div class="h4 mb-3">
        <i class="bi bi-house-add-fill"></i> Update Your {{$room->room_name}} Room
    </div>
    <form action="{{ route('owner.update', ['id' => $room->id])}}" method="post">
        @csrf
        <div class="form-row row mb-3">
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Room Name</label>
                <input type="text" class="form-control" id="input1" value="{{$room->room_name}}" name="room_name">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Location</label>
                <input type="text" class="form-control" id="input1" value="{{$room->location}}" name="location">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Floor Size</label>
                <input type="text" class="form-control" id="input1" value="{{$room->floor_size}}" name="floor_size">
            </div>
        </div>
        <div class="form-row row mb-3">
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Beds Available</label>
                <input type="text" class="form-control" id="input1" value="{{$room->no_of_beds}}" name="no_of_beds">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Minimum Availability Per Person</label>
                <input type="text" class="form-control" id="input1" value="{{$room->min_availability_per_person}}" name="min_availability_per_person">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Maximum Availability Per Person</label>
                <input type="text" class="form-control" id="input1" value="{{$room->max_availability_per_person}}" name="max_availability_per_person">
            </div>
        </div>
        <div class="form-row row mb-3">
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Room Rent (Per Day)</label>
                <input type="text" class="form-control" id="input1" value="{{$room->cost}}" name="cost">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Amenities of the Room</label>
                <input type="text" class="form-control" id="input1" value="{{$room->aminities}}" name="aminities">
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Type of Property</label>
                <input type="text" class="form-control" id="input1" value="{{$room->type_of_property}}" name="type_of_property">
            </div>
        </div>
        <div class="form-row row mb-3">
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Address</label>
                <textarea class="form-control" id="input1" name="address">{{$room->address}}</textarea>
            </div>
            <div class="form-group col-lg-4">
                <label for="input1" class="mb-2">Description</label>
                <textarea class="form-control" id="input1" name="description">{{$room->description}}</textarea>
            </div>

        </div>
        <div class="d-flex justify-content-end">
            <button class="btn btn-outline-success ml-auto mt-3" type="submit"><i class="bi bi-plus-lg"></i> Update {{$room->room_name}} Room</button>
        </div>
    </form>
</div>
@endsection