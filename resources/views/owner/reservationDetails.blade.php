@extends('layouts.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/dashboard">
            <i class="bi bi-house-lock-fill"></i> RoomBook
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown fs-5">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person-circle"></i> {{Auth::user()->username}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('owner.profile') }}">My Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('owner.reservationDetails') }}">Reservation Details</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item" type="submit"><i class="bi bi-box-arrow-left"></i> Logout</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container mt-1">
    <a href="javascript:history.go(-1)" class="text-decoration-none text-dark">
        <i class="bi bi-arrow-left me-2"></i> Back
    </a>
    <p class="mt-3 h5">Reservation Details of {{Auth::user()->username}}'s Room</p>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th>Room Name</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Check-in Date</th>
                    <th>Check-out Date</th>
                    <th>Status</th>
                    <th>Cost</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($userRooms as $room)
                @foreach ($room->bookings as $booking)
                <tr>
                    <td>{{ $room->room_name }}</td>
                    <td>{{ $booking->user->username }}</td>
                    <td>{{ $booking->user->email }}</td>
                    <td>{{ $booking->user->phone }}</td>
                    <td>{{ $booking->checkin_date }}</td>
                    <td>{{ $booking->checkout_date }}</td>
                    <td>
                        <p><span>{{ $booking->booked ? '🤩 Booked' : '🫠 Canceled' }}</span></p>
                    </td>
                    <td>{{ $booking->rent }}</td>
                </tr>
                @endforeach
                @endforeach
            </tbody>
        </table>
    </div>

</div>
@endsection