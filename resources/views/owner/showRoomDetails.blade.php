@extends('layouts.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/dashboard">
            <i class="bi bi-house-lock-fill"></i> RoomBook
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown fs-5">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person-circle"></i> {{Auth::user()->username}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('owner.profile') }}">My Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('owner.reservationDetails') }}">Reservation Details</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item" type="submit"><i class="bi bi-box-arrow-left"></i> Logout</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container mt-1">
    <a href="javascript:history.go(-1)" class="text-decoration-none text-dark">
        <i class="bi bi-arrow-left me-2"></i> Back
    </a>
    <div class="row mb-2 mt-3">
        <div class="col">
            <h2 class="mb-0"><i class="bi bi-house-check"></i> {{ $room->room_name }} Room Details</h2>
            <p class="text-secondary mt-3"><i class="bi bi-geo-alt-fill"></i> {{ $room->address }}</p>
        </div>
        <div class="col-auto">
            <span class="badge {{ $room->available ? 'bg-success' : 'bg-danger' }} p-2">
                {{ $room->available ? 'Available' : 'Not Available' }}
            </span>
        </div>
    </div>
    <div class="row mt-3 ">
        <div class="col-lg-6 mb-3 d-none d-lg-block">
            <img src="{{ asset('storage/room_images/' . $room->images[0]->image) }}" class="img-fluid rounded" alt="Room Image" style="height: 400px; width:600px;">
        </div>
        <div class="col-lg-6  d-none d-lg-block">
            <div class="row">
                @for ($i = 1; $i < 5; $i++) <div class="col-lg-6 mb-3">
                    <img src="{{ asset('storage/room_images/' . $room->images[$i]->image) }}" class="img-fluid rounded" alt="Room Image" style="height: 200px; width:300px;">
            </div>
            @endfor
        </div>
    </div>
</div>
<div class="row mt-3 d-block d-lg-none">
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            @foreach ($room->images as $index => $image)
            <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
                <img src="{{ asset('storage/room_images/' . $image->image) }}" class="d-block w-100 rounded-top" alt="Room Image" height="300px">
            </div>
            @endforeach
        </div>
    </div>
</div>
</div>
<div class="container mt-2">
    <div class="row">
        <div class="col-lg-8 mb-4">
            @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
            @endif
            <div class="card border-0 shadow-sm">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>Floor Size:</strong> {{ $room->floor_size }} feet</p>
                            <p><strong>Beds Available:</strong> {{ $room->no_of_beds }}</p>
                            <p><strong>Property Type:</strong> {{ $room->type_of_property }}</p>
                            <p><strong>Availability:</strong> {{ $room->min_availability_per_person }} to {{ $room->max_availability_per_person }} Days</p>
                        </div>
                    </div>
                    <hr>
                    <p class="h4"><i class="bi bi-basket"></i> Amenities</p>
                    <p class="text-secondary" style="text-align: justify;">{{ $room->aminities }}</p>
                    <hr>
                    <p class="h4"><i class="bi bi-three-dots"></i> Description</p>
                    <p class="text-secondary" style="text-align: justify;">{{ $room->description }}</p>
                    <hr>
                    <div class="d-flex align-items-center">
                        <button class="btn btn-outline-primary me-3" data-bs-toggle="modal" data-bs-target="#updateModal">
                            <i class="bi bi-pencil-fill"></i> Update
                        </button>
                        <button class="btn btn-outline-danger me-3" data-bs-toggle="modal" data-bs-target="#deleteModal">
                            <i class="bi bi-trash-fill"></i> Delete
                        </button>
                        <button class="btn btn-outline-dark" data-bs-toggle="modal" data-bs-target="#hideModal">
                            <i class="bi {{ $room->hide ? 'bi-eye-fill' : 'bi-eye-slash-fill' }}"></i>
                            {{ $room->available ? 'Make Unavailable' : 'Make Available' }} Room
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card border-0 shadow-sm mt-4">
                <div class="card-body">
                    <h5 class="mb-4"><i class="bi bi-calendar-check"></i> Reservation Details</h5>
                    @foreach($bookings as $booking)
                    <p>
                    <div class="h3" style="cursor: pointer;">
                        <strong class="user-link text-success" data-bs-toggle="modal" data-bs-target="#userModal{{$booking->user->id}}">
                            <i class="bi bi-person-circle"></i> {{ $booking->user->username }}
                        </strong>
                    </div>
                    was Booked From
                    <strong>
                        {{$booking->checkin_date}}
                    </strong>
                    to
                    <strong>
                        {{$booking->checkout_date}}
                    </strong>
                    </p>
                    @endforeach
                </div>
            </div>

            @foreach($bookings as $booking)
            <div class="modal fade" id="userModal{{$booking->user->id}}" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="userModalLabel">User Details</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p><strong>Name:</strong> {{ $booking->user->username }}</p>
                            <p><strong>Email:</strong> {{ $booking->user->email }}</p>
                            <p><strong>Phone:</strong> {{ $booking->user->phone }}</p>
                            <p><strong>Check In Date:</strong>
                                {{$booking->checkin_date}}
                            </p>
                            <p><strong>Check Out Date:</strong>
                                {{$booking->checkout_date}}
                            </p>
                            <p><strong>Rent:</strong> <i class="bi bi-currency-rupee"></i>{{ $booking->rent }}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="modal fade" id="hideModal" tabindex="-1" aria-labelledby="hideModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hideModalLabel">Hide Room</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to {{!$room->available ? 'Make Available' : 'Make Unavailable'}} this room?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <form action="{{ route('owner.hideRoom', $room->id) }}" method="post">
                    @csrf
                    @method('PUT')
                    <button type="submit" class="btn btn-danger">{{!$room->available ? 'Make Available' : 'Make Unavailable'}} Room</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModelLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModelLabel">Delete Room</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this room?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <form action="{{ route('owner.destroy', $room->id) }}" method="post">
                    @csrf
                    @method('PUT')
                    <button type="submit" class="btn btn-danger">Delete Room</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateModalLabel">Update Room</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to Update this room?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <form action="{{ route('owner.showUpdate', ['id' => $room->id]) }}" method="get">
                    @csrf
                    <button type="submit" class="btn btn-primary">Update Room</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection