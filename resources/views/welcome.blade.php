@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="card pb-lg-5 w-50 mx-auto shadow-sm rounded">
        <div class="text-center">
            <img src="https://images.pexels.com/photos/4825701/pexels-photo-4825701.jpeg?auto=compress&cs=tinysrgb&w=1600" alt="" class="w-100 rounded-top" height="300px">
            <h1 class="mt-4">Welcome to RoomBook</h1>
            <p>Your trusted platform for finding and booking rooms.</p>
            <div class="mt-4">
                <a href="{{ route('login') }}" class="btn btn-success">Login</a>
                <a href="{{ route('register') }}" class="btn btn-outline-secondary">Register</a>
            </div>
        </div>
    </div>
</div>

@endsection