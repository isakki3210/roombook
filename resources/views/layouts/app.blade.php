<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-beta1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">

    <title>Room Booking</title>
</head>

<body>
    <div class="container">
        @yield('content')
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-beta1/js/bootstrap.bundle.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const steps = document.querySelectorAll('.step');
            let currentStep = 0;

            function showStep(stepIndex) {
                steps.forEach((step, index) => {
                    if (index === stepIndex) {
                        step.style.display = 'block';
                    } else {
                        step.style.display = 'none';
                    }
                });
            }

            function handleNext() {
                if (currentStep < steps.length - 1) {
                    currentStep++;
                    showStep(currentStep);
                }
            }

            function handlePrev() {
                if (currentStep > 0) {
                    currentStep--;
                    showStep(currentStep);
                }
            }

            document.querySelectorAll('.next').forEach(button => {
                button.addEventListener('click', handleNext);
            });

            document.querySelectorAll('.prev').forEach(button => {
                button.addEventListener('click', handlePrev);
            });

            showStep(currentStep);
        });

        document.addEventListener('DOMContentLoaded', function() {
            const searchInput = document.getElementById('searchInput');
            const roomCards = document.querySelectorAll('.col-lg-4');

            searchInput.addEventListener('input', function() {
                const searchTerm = searchInput.value.trim().toLowerCase();

                roomCards.forEach(function(card) {
                    const roomName = card.getAttribute('data-room-name').toLowerCase();
                    const location = card.getAttribute('data-location').toLowerCase();

                    const matches = roomName.includes(searchTerm) || location.includes(searchTerm);
                    card.style.display = matches ? 'block' : 'none';
                });
            });
        });

        function getCurrentDate() {
            const now = new Date();
            const year = now.getFullYear();
            const month = (now.getMonth() + 1).toString().padStart(2, '0');
            const day = now.getDate().toString().padStart(2, '0');
            return `${year}-${month}-${day}`;
        }
        document.getElementById('checkInDate').min = getCurrentDate();
        document.getElementById('checkInDate').addEventListener('input', function() {
            document.getElementById('checkOutDate').min = this.value;
        });
    </script>
</body>

</html>