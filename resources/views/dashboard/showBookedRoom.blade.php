@extends('layouts.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/dashboard">
            <i class="bi bi-house-lock-fill"></i> RoomBook
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown fs-5">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person-circle"></i> {{Auth::user()->username}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('dashboard.profile') }}">My Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('dashboard.booked') }}">Booked Rooms</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item" type="submit"><i class="bi bi-box-arrow-left"></i> Logout</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

@php
$user = $users->where('id', $room->user_id)->first();
@endphp
<div class="container mt-1">
    <a href="javascript:history.go(-1)" class="text-decoration-none text-dark">
        <i class="bi bi-arrow-left me-2"></i> Back
    </a>
    <div class="row mb-2 mt-3">
        <div class="col">
            <h2 class="mb-0"><i class="bi bi-house-check"></i> {{ $room->room_name }}</h2>
            <p class="text-secondary mt-3"><i class="bi bi-geo-alt-fill"></i> {{ $room->address }}</p>
        </div>
        <div class="col-auto">
            <span class="badge {{ $room->available ? 'bg-success' : 'bg-danger' }} p-2">
                {{ $room->available ? 'Available' : 'Not Available' }}
            </span>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-6 mb-3 d-none d-lg-block">
            <img src="{{ asset('storage/room_images/' . $room->images[0]->image) }}" class="img-fluid rounded" alt="Room Image" style="height: 400px; width:600px;">
        </div>
        <div class="col-lg-6 d-none d-lg-block">
            <div class="row">
                @for ($i = 1; $i < 5; $i++) <div class="col-lg-6 mb-3">
                    <img src="{{ asset('storage/room_images/' . $room->images[$i]->image) }}" class="img-fluid rounded" alt="Room Image" style="height: 200px; width:300px;">
            </div>
            @endfor
        </div>
    </div>
</div>
<div class="row mt-3 d-block d-lg-none">
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            @foreach ($room->images as $index => $image)
            <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
                <img src="{{ asset('storage/room_images/' . $image->image) }}" class="d-block w-100 rounded-top" alt="Room Image" height="300px">
            </div>
            @endforeach
        </div>
    </div>
</div>

</div>
<div class="row">
    <div class="col-lg-8 mb-4">
        <div class="card border-0 shadow-sm">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <p><strong>Holder:</strong> {{ $user->username }}</p>
                        <p><strong>Type of Property:</strong> {{ $room->type_of_property }}</p>
                        <p><strong>Floor Size:</strong> {{ $room->floor_size }} feet</p>
                        <p><strong>Beds Available:</strong> {{ $room->no_of_beds }}</p>
                        <p><strong>Cost Per Day:</strong> <i class="bi bi-currency-rupee"></i>{{ $room->cost }}</p>
                        <p><strong>Availability:</strong> {{ $room->min_availability_per_person }} to {{ $room->max_availability_per_person }} Days</p>
                    </div>
                    <div class="col-md-6">
                        <p class="h6"><i class="bi bi-basket"></i> Amenities</p>
                        <p class="text-secondary" style="text-align: justify;">{{ $room->aminities }}</p>
                    </div>
                </div>
                <hr>
                <p class="h6"><i class="bi bi-three-dots"></i> More About {{$room->room_name}}</p>
                <p class="text-secondary" style="text-align: justify;">{{ $room->description }}</p>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <p class="h6"><i class="bi bi-person-circle"></i> Contact Details</p>
                        <p class="text-secondary mt-3"><i class="bi bi-telephone-minus-fill"></i> {{ $user->phone }}</p>
                        <p class="text-secondary"><i class="bi bi-envelope-at-fill"></i> {{ $user->email }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card border-0 shadow-sm mt-4">
            <div class="card-body">
                <h4 class="mb-4"><i class="bi bi-bookmark-check-fill"></i> Your Reservation Details</h4>
                @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif
                @if($booking && $booking->booked)
                <div class="col-lg-12 h5 mt-3 mx-1 text-secondary text-center">
                    <p>😍 Great news! You have successfully booked this room from: <br>
                        {{$booking->checkin_date}} to {{$booking->checkout_date}} Days
                    </p>
                    <p>Feel free to enjoy your stay! If you need any assistance, contact us.
                    </p>
                    <button class="btn btn-outline-danger me-3" data-bs-toggle="modal" data-bs-target="#deleteModal">
                        <i class="bi bi-x-octagon"></i> Cancel Reservation
                    </button>
                </div>
                @endif
                @if($booking && !$booking->booked)
                <div class="col-lg-12 h5 mt-3 mx-1 text-secondary text-center">
                    <p>🫠 Your reservation has been successfully canceled. We hope to serve you again in the future!</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModelLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModelLabel">Delete Room</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to Cancel this Reservation?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <form action="{{ route('dashboard.cancel', $booking->id) }}" method="post">
                    @csrf
                    @method('PUT')
                    <button type="submit" class="btn btn-danger">Cancel Reservation</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection