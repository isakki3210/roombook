@extends('layouts.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/dashboard">
            <i class="bi bi-house-lock-fill"></i> RoomBook
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown fs-5">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person-circle"></i> {{Auth::user()->username}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('dashboard.profile') }}">My Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('dashboard.booked') }}">Booked Rooms</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item" type="submit"><i class="bi bi-box-arrow-left"></i> Logout</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container mt-2">
    <a href="javascript:history.go(-1)" class="text-decoration-none text-dark">
        <i class="bi bi-arrow-left me-2 mx-1"></i> Back
    </a>
    <p class="h4 mb-4 mx-1 text-dark font-weight-normal mt-2"><i class="bi bi-bookmark-check-fill"></i> Reserved Spaces of {{Auth::user()->username}}</p>

    <div class="row">
        @forelse ($bookings as $booking)
        @php
        $room = $booking->room;
        $user = $users->where('id', $room->user_id)->first();
        @endphp
        <div class="col-lg-4 mb-3">
            <div class="card shadow-sm" style="width: 25rem; box-shadow: 0 0 29px 0 rgba(68, 88, 144, 0.12);">
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($room->images as $index => $image)
                        <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
                            <img src="{{ asset('storage/room_images/' . $image->image) }}" class="d-block w-100 rounded-top" alt="Room Image" height="300px">
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title"> {{ $room->room_name }}</h5>
                    <p class="card-text"><i class="bi bi-person-exclamation"></i> Room Host: {{ $user->username }}</p>
                    <p class="card-text" style="margin-top: -10px;"><i class="bi bi-geo-alt-fill"></i> {{ $room->location }}</p>
                    <p><span class="badge {{ $booking->booked ? 'bg-success' : 'bg-danger' }} p-2">{{ $booking->booked ? 'Booked' : 'Canceled' }}</span></p>
                    <a href="{{ route('dashboard.showBookedRoom', ['id' => $room->id, 'book_id' => $booking->id]) }}" class="btn btn-outline-primary stretched-link w-100"><i class="bi bi-info-circle"></i> Explore More</a>
                </div>
            </div>
        </div>
        @empty
        <div class="col-lg-12 h5 mt-3 mx-1 text-secondary text-center">
            <p>😥No rooms Booked yet. Please Book & Enjoy..</p>
        </div>
        @endforelse
    </div>
</div>
@endsection