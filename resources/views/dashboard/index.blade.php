@extends('layouts.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/dashboard">
            <i class="bi bi-house-lock-fill"></i> RoomBook
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <div class="div d-lg-block d-none me-3 mt-2">
                    <div class="input-group mb-1">
                        <input type="text" class="form-control" placeholder="Search..." id="searchInput" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <span class="input-group-text" id="basic-addon2"><i class="bi bi-search"></i></span>
                    </div>
                </div>
                <li class="nav-item dropdown fs-5">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person-circle"></i> {{Auth::user()->username}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('dashboard.profile') }}">My Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('dashboard.booked') }}">Booked Rooms</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item" type="submit"><i class="bi bi-box-arrow-left"></i> Logout</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>


<div class="container mt-3">
    <p class="h4 mb-3 mx-1 text-dark font-weight-normal">Welcome Back, {{ Auth::user()->username }} </p>
    <p class="h4 mb-3 mx-1 text-dark font-weight-normal">Available Rooms</p>
    <div class="div d-lg-none d-block me-3 mt-2 w-100 ms-2 mb-4">
        <div class="input-group mb-1">
            <input type="text" class="form-control" placeholder="Search..." id="searchInput" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <span class="input-group-text" id="basic-addon2"><i class="bi bi-search"></i></span>
        </div>
    </div>
    <div class="row mx-auto">
        @forelse ($rooms as $room)
        <div class="col-lg-4 mb-3" data-room-name="{{ $room->room_name }}" data-location="{{ $room->location }}">
            <div class="card shadow-sm rounded-3" style="width: 25rem;">
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($room->images as $index => $image)
                        <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
                            <img src="{{ asset('storage/room_images/' . $image->image) }}" class="d-block w-100 rounded-top" alt="Room Image" height="300px">
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ $room->room_name }}</h5>
                    @php
                    $user = $users->where('id', $room->user_id)->first();
                    @endphp
                    <p class="card-text"><i class="bi bi-person-exclamation"></i> Room Host: {{ $user->username }}</p>
                    <p class="card-text" style="margin-top: -10px;"><i class="bi bi-geo-alt-fill"></i> {{ $room->location }}</p>
                    <p><span class="badge {{ $room->available ? 'bg-success' : 'bg-danger' }} p-2">{{ $room->available ? 'Available Now' : 'Not Available' }}</span></p>
                    <a href="{{ route('dashboard.showRoom', $room->id) }}" class="btn btn-outline-primary stretched-link w-100"><i class="bi bi-info-circle"></i> Explore More</a>
                </div>
            </div>
        </div>
        @empty
        <div class="col-lg-12 h3 mt-3 mx-1 text-secondary text-center">
            <p>😥No rooms added yet. Please Wait</p>
        </div>
        @endforelse
    </div>
</div>
@endsection