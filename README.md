# RoomBooking Laravel Project

## Overview

The RoomBooking Laravel Project is a web application that allows users to book rooms, manage reservations, and view available spaces. This README file provides detailed information on how to set up, use, and understand the project.

## Table of Contents

1. [Installation](#installation)
2. [Usage](#usage)
3. [Pages](#pages)
4. [How It Works](#how-it-works)

## Installation

Follow these steps to set up the RoomBooking project on your local development environment:

1. **Clone the Repository**:

    ```bash
    git clone https://github.com/Isakki07/RoomBooking.git
    cd RoomBooking
    ```

2. **Install Dependencies**:

    ```bash
    composer install
    ```

3. **Create the Environment File**:

    ```bash
    cp .env.example .env
    ```

4. **Configure Database**:

    - Create a new MySQL database for the project.
    - Update the `.env` file with your database credentials:

        ```env
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=your_database_name
        DB_USERNAME=your_database_username
        DB_PASSWORD=your_database_password
        ```

5. **Migrate and Seed Database**:

    ```bash
    php artisan migrate --seed
    ```

6. **Link The Storage**:

    ```bash
    php artisan storage:link
    ```

7. **Start the Development Server**:

    ```bash
    php artisan serve
    ```

8. **Access the Application**:
   Open your web browser and navigate to [http://localhost:8000](http://localhost:8000) to access the RoomBooking application.

## Usage

-   Register for a new account or log in if you already have one.
-   Explore the available rooms and spaces.
-   Make reservations for the desired date and time.
-   View and manage your reservations from your profile.

## Pages

The RoomBooking project includes the following pages:

-   **Home**: Displays available rooms and spaces for booking.
-   **Rooms**: Lists all available rooms with details.
-   **Reservations**: Displays user reservations and allows for cancellation.
-   **Profile**: User profile management, including account settings and password change.

## How It Works

The RoomBooking application is built using Laravel PHP and follows the MVC (Model-View-Controller) architectural pattern. Here's a high-level overview of how it works:

-   **Controllers**: Handle user requests, interact with models, and return views or JSON responses.
-   **Models**: Represent data structures and interact with the database.
-   **Views**: Display the user interface and receive user input.

The application flow typically involves:

1. User registration and login.
2. Browsing available rooms and spaces.
3. Selecting a room and making a reservation.
4. Viewing and managing reservations in the user profile.

## Unique Features

-   **Intuitive Booking Process**: Our application provides an intuitive and user-friendly booking process, ensuring a seamless experience for users.

-   **Real-Time Availability**: Check the real-time availability of rooms and spaces, allowing users to make informed decisions.

-   **Customizable Profiles**: Tailor your profile with ease, managing account settings and changing passwords effortlessly.

-   **Efficient Cancellation**: The reservation system includes an efficient cancellation process, putting users in control of their bookings.

For more detailed information, refer to the codebase and comments within the project.
